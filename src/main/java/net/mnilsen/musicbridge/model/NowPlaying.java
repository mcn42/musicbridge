/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.musicbridge.model;

import java.util.Objects;

/**
 * "player_state": "playing", "volume": 60, "muted": false, "id":
 * "AC4FFD2271422B47", "name": "Forever", "artist": "HAIM", "album": "Days Are
 * Gone (2013)", "playlist": "Summer Jams"
 *
 * @author michaeln
 */
public class NowPlaying {
    private String playerState;
    private int volume;
    boolean muted;
    private String id;
    private String name;
    private String artist;
    private String album;
    private String playlist;

    public NowPlaying() {
    }

    public NowPlaying(String playerState, int volume, boolean muted, String id, String name, String artist, String album, String playlist) {
        this.playerState = playerState;
        this.volume = volume;
        this.muted = muted;
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.playlist = playlist;
    }

    public String getPlayerState() {
        return playerState;
    }

    public void setPlayerState(String playerState) {
        this.playerState = playerState;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getPlaylist() {
        return playlist;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NowPlaying other = (NowPlaying) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "NowPlaying{" + "playerState=" + playerState + ", volume=" + volume + ", muted=" + muted + ", id=" + id + ", name=" + name + ", artist=" + artist + ", album=" + album + ", playlist=" + playlist + '}';
    }
    
    
}
