/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.musicbridge.model;

import java.util.Objects;

/**
 *
 * @author michaeln
 */
public class AirplayDevice {
    private String id;
    private String name;
    private String kind;
    private boolean active;
    private boolean selected;
    private int volume;
    private boolean supportsAudio;
    private boolean supportsVideo;
    private String networkAddress;

    public AirplayDevice() {
    }

    public AirplayDevice(String id, String name, String kind, boolean active, boolean selected, int volume, boolean supportsAudio, boolean supportsVideo, String networkAddress) {
        this.id = id;
        this.name = name;
        this.kind = kind;
        this.active = active;
        this.selected = selected;
        this.volume = volume;
        this.supportsAudio = supportsAudio;
        this.supportsVideo = supportsVideo;
        this.networkAddress = networkAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public boolean isSupportsAudio() {
        return supportsAudio;
    }

    public void setSupportsAudio(boolean supportsAudio) {
        this.supportsAudio = supportsAudio;
    }

    public boolean isSupportsVideo() {
        return supportsVideo;
    }

    public void setSupportsVideo(boolean supportsVideo) {
        this.supportsVideo = supportsVideo;
    }

    public String getNetworkAddress() {
        return networkAddress;
    }

    public void setNetworkAddress(String networkAddress) {
        this.networkAddress = networkAddress;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AirplayDevice other = (AirplayDevice) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "AirplayDevice{" + "id=" + id + ", name=" + name + ", kind=" + kind + ", active=" + active + ", selected=" + selected + ", volume=" + volume + ", supportsAudio=" + supportsAudio + ", supportsVideo=" + supportsVideo + ", networkAddress=" + networkAddress + '}';
    }
    
    
}
