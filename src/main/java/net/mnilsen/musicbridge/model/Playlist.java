/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.musicbridge.model;

import java.util.Objects;

/**
 *
 * @author michaeln
 */
public class Playlist {
    private String id;
    private String name;
    private boolean loved;
    private int durationInSeconds;
    private String time;

    public Playlist() {
    }

    public Playlist(String id, String name, boolean loved, int durationInSeconds, String time) {
        this.id = id;
        this.name = name;
        this.loved = loved;
        this.durationInSeconds = durationInSeconds;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoved() {
        return loved;
    }

    public void setLoved(boolean loved) {
        this.loved = loved;
    }

    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Playlist other = (Playlist) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Playlist{" + "id=" + id + ", name=" + name + ", loved=" + loved + ", durationInSeconds=" + durationInSeconds + ", time=" + time + '}';
    }
    
    
}
