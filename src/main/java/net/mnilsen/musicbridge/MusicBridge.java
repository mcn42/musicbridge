/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package net.mnilsen.musicbridge;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author michaeln
 */
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "MacOS Music API", version = "1.0", description = "Control Music via an API"))
public class MusicBridge {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        SpringApplication.run(MusicBridge.class, args);
    }
}
