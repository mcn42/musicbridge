/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.musicbridge;

import java.awt.image.BufferedImage;
import java.util.List;
import net.mnilsen.musicbridge.model.AirplayDevice;
import net.mnilsen.musicbridge.model.NowPlaying;
import net.mnilsen.musicbridge.model.Playlist;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * API descr.:  https://github.com/maddox/itunes-api/tree/1.5.2
 * @author michaeln
 */
@RestController()
public class MusicController {

    @RequestMapping(value = "/now_playing", method = RequestMethod.GET, produces = "application/json")
    public NowPlaying nowPlaying() {
        return null;
    }
    
    @RequestMapping(value = "/artwork", method = RequestMethod.GET, produces = "image/jpeg")
    public BufferedImage artwork() {
        return null;
    }
    
    @RequestMapping(value = "/playpause", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying playPause() {
        return null;
    }
    
    @RequestMapping(value = "/stop", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying stop() {
        return null;
    }
    
    @RequestMapping(value = "/previous", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying previous() {
        return null;
    }
    
    @RequestMapping(value = "/next", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying next() {
        return null;
    }
    
    @RequestMapping(value = "/play", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying play() {
        return null;
    }
    
    @RequestMapping(value = "/pause", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying pause() {
        return null;
    }
    
    @RequestMapping(value = "/vaolume/{level}", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying voulume(int level) {
        return null;
    }
    
    @RequestMapping(value = "/vaolume/{muted}", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying voulume(boolean muted) {
        return null;
    }
    
    @RequestMapping(value = "/playlists", method = RequestMethod.GET, produces = "application/json")
    public List<Playlist> playlists() {
        return null;
    }
    
    @RequestMapping(value = "/playlists/{id}/play", method = RequestMethod.PUT, produces = "application/json")
    public NowPlaying playlist() {
        return null;
    }
    
    @RequestMapping(value = "/airplay_devices", method = RequestMethod.GET, produces = "application/json")
    public List<AirplayDevice> airplayDevices() {
        return null;
    }
    
    @RequestMapping(value = "/airplay_devices/{id}", method = RequestMethod.GET, produces = "application/json")
    public AirplayDevice airplayDevice(String id) {
        return null;
    }
    
    @RequestMapping(value = "/airplay_devices/{id}/on", method = RequestMethod.PUT, produces = "application/json")
    public AirplayDevice airplayDeviceOn(String id) {
        return null;
    }
    
    @RequestMapping(value = "/airplay_devices/{id}/off", method = RequestMethod.PUT, produces = "application/json")
    public AirplayDevice airplayDeviceOff(String id) {
        return null;
    }
    
    @RequestMapping(value = "/airplay_devices/{id}/volume/{vol}", method = RequestMethod.PUT, produces = "application/json")
    public AirplayDevice airplayDeviceVol(int vol) {
        return null;
    }
    
}
