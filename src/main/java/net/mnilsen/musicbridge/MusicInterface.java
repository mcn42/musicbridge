/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.mnilsen.musicbridge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import net.mnilsen.musicbridge.model.AirplayDevice;
import net.mnilsen.musicbridge.model.NowPlaying;
import net.mnilsen.musicbridge.model.Playlist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author michaeln
 */
public class MusicInterface {

    Logger logger = LoggerFactory.getLogger(MusicInterface.class);
    private AtomicBoolean activated = new AtomicBoolean(false);
    
    public MusicInterface() {
    }
    
    private void activate() {
        if (this.activated.get()) {
            return;
        }
    }
    
    private List<Property> getProperties(String propsString) {
        List<Property> props = new ArrayList<>();
        
        return props;
    }
    
    public String executeCommand(Command comm, String param1) throws CommandExecutionException {
        StringBuilder sb = new StringBuilder();
        String command = (param1 == null || param1.isBlank()) ? comm.getCommand() : String.format(comm.getCommand(), param1);
        try {          
            logger.debug(String.format("Executing command %s...", command));
            Process proc = new ProcessBuilder().command(command).start();
            final BufferedReader stdoutReader
                    = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = stdoutReader.readLine();
            while (line != null) {
                sb.append(line).append("\n");
                line = stdoutReader.readLine();
            }
            if(proc.exitValue() != 0) {
                logger.error(String.format("Bad return code of %d", proc.exitValue()));
                throw new CommandExecutionException(String.format("Bad return code of %d", proc.exitValue()));
            }
        } catch (IOException ex) {
            throw new CommandExecutionException(String.format("Could not execute command '%s'",command), ex);
        }
        logger.debug(String.format("Command returned '%s'", sb.toString()));
        return sb.toString();
    }
    
    public NowPlaying executeNPCommand(Command comm) throws CommandExecutionException {
        return null;
    }
    
    public AirplayDevice executeAPDCommand(Command comm, AirplayDevice apdIn) throws CommandExecutionException {
        return null;
    }
    
    public Playlist executePLCommand(Command comm, String plId) throws CommandExecutionException {
        return null;
    }
    
    public List<Playlist> getAllPlaylists() throws CommandExecutionException {
        List<Playlist> list = null;
        
        return list;
    }
    
    public List<AirplayDevice> getAllAirplayDevices() throws CommandExecutionException {
        List<AirplayDevice> list = null;
        
        return list;
    }

    public class Property {

        private final String name;
        private final String value;
        
        public Property(String name, String value) {
            this.name = name;
            this.value = value;
        }
        
        public String getName() {
            return name;
        }
        
        public String getValue() {
            return value;
        }
        
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.name);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Property other = (Property) obj;
            return Objects.equals(this.name, other.name);
        }
        
        @Override
        public String toString() {
            return "Property{" + "name=" + name + ", value=" + value + '}';
        }
        
    }
    
}
