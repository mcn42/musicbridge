/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package net.mnilsen.musicbridge;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author michaeln
 */
public enum Command {
    ACTIVATE("'tell application \"Music\" to activate'"),
    PLAY("'tell application \"Music\" to play'"),
    CURRENT_TRACK("tell application \"Music\" to get properties of current track");
    
    private String command;

    private Command(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
    
    
}
